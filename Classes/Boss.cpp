//
//  Boss.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 19/9/14.
//
//

#include "Boss.h"
#include "Player.h"
#include "ShooterScene.h"
#include "BossBullet.h"
bool Boss::init()
{
	if (initWithSpriteFrameName("enemyufo.png"))
	{
		health = 30; //setting the health to be higher
		setAnchorPoint(Vec2(0.5,0.5));
		amIDestroyed = false;
		setPosition(Vec2(-getContentSize().width/2, 568-getContentSize().height/2));
		timeElapsed = 0;
		setLocalZOrder(200);
		schedule(schedule_selector(Boss::updateState));
		currentState = STATE_ATTACKING; //current state as attacking

		return true;
	}
	return false;
}

void Boss::hitByPlayer()
{
	//empty, because we dun want anything to happen when it is hit by player
}

void Boss::hitByBullet(Bullet* bullet)
{
	// calling the super class hitbyBullet method
	// as the logic is the same. we override this function
	// to perform additional logics special to Boss
	Enemy::hitByBullet(bullet);
	
	//a standard tinting sequence
	Sequence* seq = Sequence::create(TintTo::create(0.1f, 255, 0, 0), TintTo::create(0.1f, 255, 255, 255), NULL);
	runAction(seq);
	
	if (health < 5)
	{
		changeToState(STATE_DYING);
	}
	else if (health < 15)
	{
		changeToState(STATE_RAGE);
	}
	
}

// updating the boss' state per frame update
void Boss::updateState(float dt)
{
	totalTimeElapsed += dt;
	switch (currentState) {
  		case STATE_ATTACKING:
			shoot(dt);
			if (totalTimeElapsed >= 7.f) //rest every 7 sec of attacking
			{
				
				changeToState(STATE_RESTING);
				totalTimeElapsed = 0;
			}
			break;
		case STATE_RESTING:
		{
			if (totalTimeElapsed >= 2.f) //attack after resting for 2 sec
			{
				changeToState(STATE_ATTACKING);
				totalTimeElapsed = 0;
			}
		}
			break;
		case STATE_RAGE: //if health is 50%, shoot more.
		{
			rageShoot(dt);
		}
			break;
		case STATE_DYING: //kamikaze the player upon dying
		{
			if (totalTimeElapsed >= 1.f) //doing it every second
			{
				moveToPlayer();
				totalTimeElapsed = 0;
			}
			
		}
			break;
		default:
			break;
	}
}

// this function performs the state change. while it looks simple in the implementation,
// more complex implementation may also check if nextState can be transited.
void Boss::changeToState(int nextState)
{
	if (currentState != nextState) //we only want to activate the state change logic if it is a different state
	{
		currentState = nextState;
		timeElapsed = 0;
		totalTimeElapsed = 0;
	}
}

// defines the way which the Boss moves
void Boss::move()
{
	// we want the Boss to move left right
	MoveBy* moveBy = MoveBy::create(2.f, Vec2(320 + getContentSize().width, 0));
	MoveBy* reverse = moveBy->reverse();
	Sequence* sequence = Sequence::create(moveBy, reverse, NULL);
	RepeatForever* repeatForever = RepeatForever::create(sequence);
	runAction(repeatForever);
}

// kamikaze towards the player ship
void Boss::moveToPlayer()
{
	stopAllActions();
	ShooterScene* myParentScene = (ShooterScene*) getParent();
	Player* player = myParentScene->getPlayer(); //track the player
	if (player)
	{
		runAction(MoveTo::create(0.8f, player->getPosition())); //move to current known player position
	}
}
void Boss::rageShoot(float dt)
{
	// in this case, we take dt and multiply it by 3. this is a form of 'cheating'
	// as it actually calls the normal shoot function. but by increasing the dt passed in
	// we can reach the shooting condition earlier, thus creating a "shooting 3 times more"
	// scenario
	shoot(dt*3);
}

void Boss::shoot(float dt)
{
	timeElapsed += dt;
	
	if (timeElapsed >= 3.f) //shoots every 3 seconds
	{
		timeElapsed = 0;
		ShooterScene* myParentScene = (ShooterScene*) getParent();
		Player* player = myParentScene->getPlayer();
		if (player)
		{
			//get the directional vector between the boss and the ship
			Vec2 direction = player->getPosition() - getPosition();
			
			//normalize the vector
			direction.normalize();
			
			//multiply the vector, that is the "speed" of the bullet
			direction *= 120;
			
			BossBullet* bb = BossBullet::create();
			bb->setPosition(getPosition());
			myParentScene->registerBossBullet(bb); //register to shootscene
			bb->setMoveVector(direction); //set direction and speed
			myParentScene->addChild(bb);
		}
	}
}