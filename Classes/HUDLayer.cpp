//
//  HUDLayer.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 7/9/14.
//
//

#include "HUDLayer.h"

bool HUDLayer::init()
{
	Menu* menu = Menu::create();
	Sprite* sprite = Sprite::createWithSpriteFrameName("button_black_pause.png");
	Sprite* sprite2 = Sprite::createWithSpriteFrameName("button_black_pause_press.png");
	MenuItemSprite* pauseBtn = MenuItemSprite::create(sprite, sprite2);
	pauseBtn->setPosition(Vec2(140, -264));
	menu->addChild(pauseBtn);
	addChild(menu);
	
	lifeLeft = 3;

	for (int i = 0; i < lifeLeft; i++)
	{
		Sprite* sprite = Sprite::createWithSpriteFrameName("life.png");
		float posX = i * sprite->getContentSize().width + 20;
		sprite->setPosition(Vec2(posX, 15));
		addChild(sprite);
		lifeSprite.pushBack(sprite);
	}
	
	
	return true;
}

void HUDLayer::reduceLife()
{
	if (lifeLeft <= 0)
	{
		return;
	}
	Sprite* sprite = lifeSprite.at(lifeLeft-1);
	sprite->setVisible(false);
	lifeLeft--;
	
	
}