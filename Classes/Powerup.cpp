//
//  Powerup.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 13/10/14.
//
//

#include "Powerup.h"
#include "ShooterScene.h"
bool Powerup::init()
{
	used = false;
	return true;

}

void Powerup::move()
{
	MoveBy* moveBy = MoveBy::create(10.f, Vec2(0, -568 - getContentSize().height/2));
	ShooterScene* myParentScene = (ShooterScene*)getParent();
	
	CallFunc* callFunc = CallFunc::create(CC_CALLBACK_0(ShooterScene::deregisterPowerup, myParentScene, this));
	Sequence* seq = Sequence::create(moveBy, callFunc, NULL);
	runAction(seq);
}