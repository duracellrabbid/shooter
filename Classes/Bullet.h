//
//  Bullet.h
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#ifndef __Shooter__Bullet__
#define __Shooter__Bullet__

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;
//Bullet as a sub-class of Sprite.

class Bullet : public Sprite {
	CC_SYNTHESIZE(bool, isExpended, IsExpended);
	
	// give the bullet a damage
	// if you implement powerup , you can use this
	// to increase the damage of your bullets
	CC_SYNTHESIZE_READONLY(int, damage, Damage);
public:
	CREATE_FUNC(Bullet);
	virtual bool init();
	void startMoving();

};

#endif /* defined(__Shooter__Bullet__) */
