//
//  Player.cpp
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#include "Player.h"
#include "Bullet.h"
#include "ShooterScene.h"
#include "SimpleAudioEngine.h"

bool Player::init()
{
	
	if (initWithSpriteFrameName("player0.png"))
	{

		setAnchorPoint(Vec2(0.5,0.5));

		
		// add event handler, should be familiar by now
		EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);
		listener->onTouchBegan = CC_CALLBACK_2(Player::touchBegan, this);
		listener->onTouchMoved = CC_CALLBACK_2(Player::touchMoved, this);
		listener->onTouchEnded = CC_CALLBACK_2(Player::touchEnded, this);
		
		getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
		
		directionalState = DIRECTION_NEUTRAL; //initial directional state is neutral
		
		// we want the ship to shoot automatically at a certain interval.
		schedule(schedule_selector(Player::shootBullet));
		
		// setting the ZOrder to ensure that the ship is at the front
		// of bullets
		setLocalZOrder(100);
		
		
		rateOfFire = 1/4.0; //ROF as 4 bullets per second
		timeElapsed = 0.f;
		
		SimpleAudioEngine::getInstance()->preloadEffect("sfx_shoot.wav"); //preload sound effect
		
		startInvincibility(5.f);
		
		return true;
	}
	return false;
}

void Player::startInvincibility(float duration)
{
	invincible = true;
	Blink* blink = Blink::create(duration, duration * 2);
	runAction(Sequence::create(blink, CallFunc::create(CC_CALLBACK_0(Player::setInvincible, this, false)), NULL));
}

void Player::shootBullet(float dt)
{
	timeElapsed += dt; //increment the time
	
	// if the timeElapsed is more or equal to rof
	// the ship will shoot.
	if (timeElapsed >= rateOfFire)
	{
		// creating the bullet
		Bullet* bullet = Bullet::create();
		
		// for now, our bullet will launch from the ship's middle,
		// thus its position will be the ship's position as well
		bullet->setPosition(getPosition());
		
		// add the bullet as a child of the ship's parent. In this
		// instance, it should be ShooterScene. This is because we
		// want the ship and bullets to share the same parent scene.
		
		/*
		 A more graphical depiction of the scene graph will be:
		 
		 ShooterScene
		 |- Player
		 |- Bullets
		 |- Enemy
		 
		 Remember, we want our ShooterScene to manage all the moving
		 objects in the scene. Hence, all movables need to be children
		 of ShooterScene.
		 
		 */
		
		if (getParent())
		{
			getParent()->addChild(bullet);
			//registering the bullet to the shooter scene
			((ShooterScene*) getParent())->registerBullet(bullet);
			bullet->startMoving();
		}
		
		SimpleAudioEngine::getInstance()->playEffect("sfx_shoot.wav");
		timeElapsed = 0;
	}
}

bool Player::touchBegan(Touch* touch, Event* event)
{
	//your standard drag and drop code

	Vec2 originalPos = touch->getLocation();
	Vec2 locationInSpace = convertToNodeSpace(originalPos);
	Rect rect = Rect(0,0, getContentSize().width, getContentSize().height);
	lastTouchPos = originalPos;
	if (rect.containsPoint(locationInSpace))
	{
		return true;
	}
	return false;
	
}



void Player::touchMoved(Touch* touch, Event* event)
{
	//your standard drag and drop code

	Vec2 touchLocation = touch->getLocation(); //getting the position which the touch event occurs
	
	Vec2 difference = touchLocation - lastTouchPos;
	setPosition(getPosition() + difference);
	lastTouchPos = touchLocation;
	
	// track if dragging left or right.
	if (difference.x < 0)
	{
		switchDirectionalState(DIRECTION_LEFT);
	}
	else
	{
		switchDirectionalState(DIRECTION_RIGHT);
	}
	
	
}

void Player::touchEnded(Touch* touch, Event* event)
{
	switchDirectionalState(DIRECTION_NEUTRAL); //switch back to neutral state when stop dragging
}

void Player::die()
{
	ParticleSystemQuad *particle = ParticleSystemQuad::create("explosion.plist");
	particle->setPosition(getPosition());
	getParent()->addChild(particle);
	particle->setAutoRemoveOnFinish(true);
}

void Player::switchDirectionalState(int newState)
{
	//only change state if different state
	if (directionalState != newState)
	{
		//display sprite based on state
		string spriteName = "player" + to_string(newState) + ".png";
		
		//get the sprite based on frame name
		SpriteFrame* frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(spriteName.c_str());

		setSpriteFrame(frame);
		
		directionalState = newState;
	}
}