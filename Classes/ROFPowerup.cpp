//
//  ROFPowerup.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 13/10/14.
//
//

#include "ROFPowerup.h"

bool ROFPowerup::init()
{
	if (initWithSpriteFrameName("lasergreenshot.png"))
	{
		return true;
	}
	return false;
}

void ROFPowerup::collected(Player* player)
{
	float currentROF = player->getROF();
	currentROF *= 0.9;
	currentROF = MAX(currentROF , 1/8.0); //capping the ROF
	player->setROF(currentROF);
}