//
//  Player.h
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#ifndef __Shooter__Player__
#define __Shooter__Player__

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;
using namespace CocosDenshion;

// directional state to track the ship's direction
// used with switchDirectionalState() to give the ship a
// more realistic movement
enum Player_Direction {
	DIRECTION_NEUTRAL = 0,
	DIRECTION_LEFT = 1,
	DIRECTION_RIGHT = 2
};
/*
 The Player class will be a sub-class of Sprite.
 */
class Player : public Sprite {
	
	
protected:
	
	// Looks familiar?? Yes, we will be recycling the
	// drag and drop code from the badger game, hence
	// this variable.
	Vec2 lastTouchPos;
	
	int directionalState; //track the current directional state
	
	void switchDirectionalState(int newState);
	CC_SYNTHESIZE(float, rateOfFire, ROF); //ROF
	float timeElapsed;
	CC_SYNTHESIZE(bool, invincible, Invincible);
public:
	CREATE_FUNC(Player);
	virtual bool init();
	bool touchBegan(Touch* touch, Event* event);
	void touchMoved(Touch* touch, Event* event);
	void touchEnded(Touch* touch, Event* event);
	void shootBullet(float dt);
	void die();
	void startInvincibility(float duration);

};
#endif /* defined(__Shooter__Player__) */
