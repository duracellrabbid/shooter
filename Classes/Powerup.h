//
//  Powerup.h
//  Shooter-Main
//
//  Created by Alexander Lee on 13/10/14.
//
//

#ifndef __Shooter_Main__Powerup__
#define __Shooter_Main__Powerup__

#include <stdio.h>
#include "cocos2d.h"
#include "Player.h"
using namespace cocos2d;
using namespace std;

/*
 Powerup is an example of abstract class. 
 
 From http://www.cplusplus.com/doc/tutorial/polymorphism/
 They are classes that can only be used as base classes, and thus are allowed to have virtual member functions without definition (known as pure virtual functions). The syntax is to replace their definition by =0 (and equal sign and a zero):
 
 
 */

class Powerup : public Sprite
{
	CC_SYNTHESIZE(bool, used, Used);
public:
	virtual bool init();
	virtual void collected(Player* player) = 0; //abstract function
	virtual void move();
};

#endif /* defined(__Shooter_Main__Powerup__) */
