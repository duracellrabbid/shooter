//
//  Enemy.cpp
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#include "Enemy.h"
#include "ShooterScene.h"
#include "ROFPowerup.h"
bool Enemy::init()
{
	if (initWithSpriteFrameName("enemyship.png"))
	{
		health = 1;
		setAnchorPoint(Vec2(0.5,0.5));
		amIDestroyed = false;
		return true;
	}
	return false;
}

void Enemy::hitByBullet(Bullet* bullet)
{
	
	// reduce health based on bullet damage
	health -= bullet->getDamage();
	if (health <= 0)
	{
		die();
	}
	
	
	
}

// function to handle when hit by player
void Enemy::hitByPlayer()
{
	health -= 1;
	if (health <= 0)
	{
		die();
	}
}

bool Enemy::checkHitByBullet(Bullet* bullet)
{
	if (getIsDestroyed())
	{
		return false;
	}
	
	
	Rect myBoundingBox = getBoundingBox();
	Rect enemyBoundingBox = bullet->getBoundingBox();

	return myBoundingBox.intersectsRect(enemyBoundingBox);
}

void Enemy::die()
{
	//Play the explosion effect when die
	ParticleSystemQuad *particle = ParticleSystemQuad::create("explosion.plist");
	particle->setPosition(getPosition());
	getParent()->addChild(particle);
	particle->setAutoRemoveOnFinish(true);
	
	// in this function, also handle the deregistering and flagging for destruction
	amIDestroyed = true;
	ShooterScene* myParentScene = (ShooterScene*)getParent();
	myParentScene->deregisterEnemey(this);
	postDeath();
}

void Enemy::postDeath()
{
	
	// for the normal enemy, we will spawn power ups
	float randomFloat = CCRANDOM_0_1(); // random chance
	if (randomFloat < 0.5f) //spawn powerup at 49%
	{
		ROFPowerup* powerup = ROFPowerup::create();
		powerup->setPosition(getPosition());
		powerup->setAnchorPoint(Vec2(0.5,0.5));
		ShooterScene* myParentScene = (ShooterScene*)getParent();
		myParentScene->registerPowerup(powerup);
		myParentScene->addChild(powerup);
		powerup->move();
	}
}

void Enemy::move()
{
	//we want the enemy to disappear after moving out of the screen
	MoveBy* moveBy = MoveBy::create(10.f, Vec2(0, -568 - getContentSize().height/2));
	ShooterScene* myParentScene = (ShooterScene*)getParent();
	
	//asking the shooter scene to deregister the enemy
	CallFunc* callFunc = CallFunc::create(CC_CALLBACK_0(ShooterScene::deregisterEnemey, myParentScene, this));
	Sequence* seq = Sequence::create(moveBy, callFunc, NULL);
	runAction(seq);
}

