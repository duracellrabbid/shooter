//
//  BossBullet.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 19/9/14.
//
//

#include "BossBullet.h"
#include "ShooterScene.h"
bool BossBullet::init()
{
	if (initWithSpriteFrameName("meteorsmall.png"))
	{
		isExpended = false;
		schedule(schedule_selector(BossBullet::move));
		
		ParticleSystemQuad* particle = ParticleSystemQuad::create("fire_particle.plist");
		particle->setPosition(Vec2(getContentSize().width/2, getContentSize().height/2));
		addChild(particle);

		return true;
	}
	return false;
}


// we schedule an update to move the boss bullet by movedir / sec.
// this function also check if it is still within the scene, if no
// it will remove itself.
void BossBullet::move(float dt)
{
	Vec2 newPos = getPosition() + (moveDir * dt);
	setPosition(newPos);
	//by default this should be the screen bound.
	Rect sceneBound = Rect(0,0, 320,568);
	if (!sceneBound.containsPoint(newPos))
	{
		ShooterScene* myParentScene = (ShooterScene*) getParent();
		myParentScene->deregisterBossBullet(this); //deregister the boss bullet
		unschedule(schedule_selector(BossBullet::move));

	}
}

