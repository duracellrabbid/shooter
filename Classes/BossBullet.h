//
//  BossBullet.h
//  Shooter-Main
//
//  Created by Alexander Lee on 19/9/14.
//
//

#ifndef __Shooter_Main__BossBullet__
#define __Shooter_Main__BossBullet__

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;


// Unlike Boss, Boss bullet is not inherited from
// Bullet class. take note that due to target differences,
// it may be easier to define it as a separate class rather
// than subclassing from Bullet.

// At the end of the day, it depends on your overall code design
// and you have to decide if subclassing from Bullet may or may not
// be beneficial

class BossBullet : public Sprite {
	CC_SYNTHESIZE(bool, isExpended, IsExpended);
	CC_SYNTHESIZE(Vec2, moveDir, MoveVector);
public:
	CREATE_FUNC(BossBullet);
	virtual bool init();
	void move(float dt);
};
#endif /* defined(__Shooter_Main__BossBullet__) */
