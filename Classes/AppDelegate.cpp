#include "AppDelegate.h"
#include "ShooterScene.h"
#include "MainMenuScreen.h"
USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("My Game");
        director->setOpenGLView(glview);
    }
	
//	Size screenSize = glview->getFrameSize();
//	if (screenSize.equals(Size(640,1136)) )
//	{
//		//we wanna get our assets from ios folder
//		FileUtils::getInstance()->addSearchPath("ios");
//		
//		//we using non-retina point coordinate system
//		glview->setDesignResolutionSize(320, 568, ResolutionPolicy::FIXED_WIDTH);
//		//assuming your assets are scaled for retina
//		glview->setContentScaleFactor(2.f);
//
//	}
//	else
//	{
//		//..... other design resolution
//	}
	
	

	glview->setDesignResolutionSize(320, 568, ResolutionPolicy::SHOW_ALL);
	
	//Defaulting the pixelformat to RGBA4444
	Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA4444);
	
	
    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    Scene *scene = Scene::create();
	scene->addChild(MainMenuScreen::create());
	

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
     SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
     SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
