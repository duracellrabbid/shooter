//
//  Bullet.cpp
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#include "Bullet.h"
#include "ShooterScene.h"
bool Bullet::init()
{
	if (initWithSpriteFrameName("lasergreen.png"))
	{
		
		damage = 1; 
		isExpended = false;
		
		setAnchorPoint(Vec2(0.5,0.5));

		return true;
		
	}
	return false;
}

void Bullet::startMoving()
{
	//Start moving the bullet. We want our bullets to move by
	//568 pixels before it self-destruct.
	MoveBy* moveTo = MoveBy::create(0.5, Vec2(0, 568));
	
	//This is the action to set a callback after an action
	ShooterScene* myParentScene = (ShooterScene*)getParent();
	
	//To run the callback after the move action, we use a Sequence
	//action to string them together. Note that the last parameter
	//will always be NULL. Long story short, that is to tell
	//the Sequence creation that it is the termination of the action
	//sequence.
	CallFunc* callFunc = CallFunc::create(CC_CALLBACK_0(ShooterScene::deregisterBullet, myParentScene, this));
	Sequence* sequence = Sequence::create(moveTo, callFunc, NULL);
	runAction(sequence);
}


