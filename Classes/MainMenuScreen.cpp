//
//  MainMenuScreen.cpp
//  Shooter-Main
//
//  Created by Alexander Lee on 9/9/14.
//
//

#include "MainMenuScreen.h"
#include "ShooterScene.h"
#include "SimpleAudioEngine.h"

bool MainMenuScreen::init()
{
	TMXTiledMap* tileMap = TMXTiledMap::create("background.tmx");
	tileMap->setAnchorPoint(Vec2(0.5,0.5));
	tileMap->setPosition(Vec2(160,284));
	addChild(tileMap);
	
	
	Label* label = Label::createWithBMFont("arial-unicode-26.fnt", "Tap To Begin");
	label->setHorizontalAlignment(TextHAlignment::CENTER);
	label->setAnchorPoint(Vec2(0.5,0.5));
	label->setPosition(Vec2(160,284));
	addChild(label);
	
	EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(MainMenuScreen::tapToBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("bgm.mp3");
	SimpleAudioEngine::getInstance()->playBackgroundMusic("bgm.mp3", true);
	
	
	return true;
}

bool MainMenuScreen::tapToBegan(Touch* touch, Event* event)
{
	Scene* scene = Scene::create();
	scene->addChild(ShooterScene::create());
	Director::getInstance()->replaceScene(scene);
	return true;
}

