//
//  ROFPowerup.h
//  Shooter-Main
//
//  Created by Alexander Lee on 13/10/14.
//
//

#ifndef __Shooter_Main__ROFPowerup__
#define __Shooter_Main__ROFPowerup__

#include <stdio.h>
#include "Powerup.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

// this powerup increase the ROF of the player ship
class ROFPowerup : public Powerup
{
public:
	CREATE_FUNC(ROFPowerup);
	virtual bool init();
	virtual void collected(Player* player);
};

#endif /* defined(__Shooter_Main__ROFPowerup__) */
