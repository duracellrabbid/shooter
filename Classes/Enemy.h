//
//  Enemy.h
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#ifndef __Shooter__Enemy__
#define __Shooter__Enemy__

#include <iostream>
#include "cocos2d.h"
#include "Bullet.h"
using namespace cocos2d;
using namespace std;
using namespace CocosDenshion;
class Enemy : public Sprite {
	
	CC_SYNTHESIZE(bool, amIDestroyed, IsDestroyed); //flag for destruction
	CC_SYNTHESIZE_READONLY(int, health, Health); //track the health of the Enemy
	virtual void postDeath(); //do something after death

public:
	CREATE_FUNC(Enemy); 
	virtual bool init();
	virtual void hitByBullet(Bullet* bullet);
	virtual bool checkHitByBullet(Bullet* bullet);//check if enemy is hit by bullet
	virtual void move();//function to move the enemy
	virtual void die();
	virtual void hitByPlayer();
};
#endif /* defined(__Shooter__Enemy__) */
