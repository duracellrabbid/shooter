//
//  GameOverScreen.h
//  Shooter-Main
//
//  Created by Alexander Lee on 9/9/14.
//
//

#ifndef __Shooter_Main__GameOverScreen__
#define __Shooter_Main__GameOverScreen__

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

class GameOverScreen : public Node
{
public:
	CREATE_FUNC(GameOverScreen);
	virtual bool init();
};

#endif /* defined(__Shooter_Main__GameOverScreen__) */
