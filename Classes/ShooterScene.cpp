//
//  ShooterScene.cpp
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#include "ShooterScene.h"
#include "Boss.h"
#define BG_SCROLLRATE -100.f //100 px per second

bool ShooterScene::init()
{
	
	// since we export our background as RGB565, it would be better to
	// load it in RGB565 format first.
	Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGB565);
	
	// loading the tilemap as background. This will serve as our
	// level marker to determine when the boss is coming out.
	backgroundLayer =TMXTiledMap::create("background.tmx");
	backgroundLayer->setAnchorPoint(Vec2(0,0));
	backgroundLayer->setPosition(Vec2::ZERO);
	addChild(backgroundLayer);
	

	
	
	
	setContentSize(Size(320,568));
	
	// We want to set the pixel format back to RGBA4444 before we load our
	// spritesheets.
	Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA4444);

	// loading the spritesheet to the memory
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("shooter_spritesheet.plist");
	
	respawnPlayer();
	
	//schedule the timer to spawn enemy and check collisions and scroll background
	schedule(schedule_selector(ShooterScene::update));
	schedule(schedule_selector(ShooterScene::spawnEnemy));
	schedule(schedule_selector(ShooterScene::scrollBackground));
	timeElapsed = 0;
	
	hud = HUDLayer::create();
	hud->setLocalZOrder(300);
	addChild(hud);
	
	return true;
}

void ShooterScene::spawnBoss()
{
	Boss* boss = Boss::create();
	addChild(boss);
	enemyList.pushBack(boss);
	boss->move();
}

void ShooterScene::scrollBackground(float dt)
{

	Vec2 newPos = backgroundLayer->getPosition() + Vec2(0, dt * BG_SCROLLRATE);
	if (newPos.y <= -(backgroundLayer->getContentSize().height - 568))
	{
		newPos.y = -(backgroundLayer->getContentSize().height - 568);
		unschedule(schedule_selector(ShooterScene::scrollBackground));
		spawnBoss();
		unschedule(schedule_selector(ShooterScene::spawnEnemy));

	}
	backgroundLayer->setPosition(newPos);
}

void ShooterScene::respawnPlayer()
{
	player = Player::create();
	player->setPosition(Vec2(160, 40));
	addChild(player);
}

void ShooterScene::deregisterBullet(Bullet* bullet)
{
	expendedBullets.pushBack(bullet);
	bullet->setIsExpended(true);
}

void ShooterScene::deregisterBossBullet(BossBullet* bullet)
{
	expendedBossBullets.pushBack(bullet);
	bullet->setIsExpended(true);
}

void ShooterScene::deregisterEnemey(Enemy* enemy)
{
	destroyedEnemies.pushBack(enemy);
	enemy->setIsDestroyed(true);
}

void ShooterScene::deregisterPowerup(Powerup* powerup)
{
	usedPowerups.pushBack(powerup);
	powerup->setUsed(true);
}

void ShooterScene::spawnEnemy(float dt)
{
	
	
	timeElapsed += dt;
	if (timeElapsed >= 2.f)
	{
		timeElapsed = 0;
		Enemy* enemy = Enemy::create();
		
		// this set of code is just to calculate the starting position
		// of the enemies. it start with just above the screen with varying
		// x-position. The formula to calculate the x position is as followed:
		
		/*
		 
		 Minimum starting x position (x1) = enemy sprite width / 2
		 Maximum starting x position (x2) = screen width - enemy sprite width / 2
		 The distance range (d) = x2 - x1
		 Starting position = x1 + (a random value from 0 to d)
		 
		 Example:
		 Sprite width = 40px
		 Screen width = 320px
		 
		 x1 = 20
		 x2 - 320 - 20 = 300
		 d = 300 - 20 = 280
		 
		 Starting position = 20 + (Random between 0 to 280)
		 
		 */
		
		float y = getContentSize().height + enemy->getContentSize().height/2;
		float x = enemy->getContentSize().width/2 + (rand() % (int)(getContentSize().width - enemy->getContentSize().width));
		enemy->setPosition(Vec2(x,y));
		
		addChild(enemy);
		enemy->move();

		enemyList.pushBack(enemy);
		
		
	}
}

// move the player die and respawn logic to a function so that it can be reused by one call
void ShooterScene::killPlayer()
{
	if (!player->getInvincible())
	{
		player->die();
		hud->reduceLife();
		player->removeFromParent();
		respawnPlayer();
	}
}

/*
 In this function, you might see a strange thing. Why do we need to
 remove the object from their respective list? Shouldnt removeFromParent
 remove them completely from the memory??
 
 This is the "magic" in cocos2d-x memory management. Every Ref object in
 coco2d-X has a reference count. When it is created by the default create
 method, it is an auto-release object which means it has a reference count
 of 1 but will be reduced to zero when the next frame updates. It will
 only be deleted from the memory after the ref count turns 0
 
 When we do an "addChild", we are adding 1 to the reference count. By
 adding the object to a Vector, you are also adding 1 to the ref count.
 
 Below is a ref count for a standard bullet object in our scene:
 
 Stage					Ref count			Remark
 Creation				1					Autorelease
 Add to parent			2
 Add to array			3
 Next frame update		2					Autorelease takes place
 Remove from parent		1
 Remove from vector		0					Will delete from memory
 
 */
void ShooterScene::update(float dt)
{
	
	//looping the enemy list
	for (int j = 0; j < enemyList.size(); j++)
	{
		Enemy* enemy = enemyList.at(j);
		
		//looping the bullet list
		for (int i = 0; i < bulletList.size(); i++)
		{
			Bullet* bullet = bulletList.at(i);
			if (!bullet->getIsExpended()) //only check if not expended
			{
				//check for bounding box intersection
				if (enemy->checkHitByBullet(bullet))
				{
					//if hit, place bullets to trash can
					expendedBullets.pushBack(bullet);
					enemy->hitByBullet(bullet); //process hit
					
				}
			}
		}
		
		if (enemy->getIsDestroyed() == false) //if not destroyed, then see if hit player ship
		{
			if (enemy->getBoundingBox().intersectsRect(player->getBoundingBox()))
			{
				//if hit, remove current player ship from parent
				//and respawn
				killPlayer();
				
				enemy->hitByPlayer();
	
			}
		}
	}
	
	// now doing the checking for Boss Bullet the logic is fairly similar to the above
	for (int i = 0; i < bossBulletList.size(); i++)
	{
		BossBullet* bullet = bossBulletList.at(i);
		if (!bullet->getIsExpended())
		{
			if (bullet->getBoundingBox().intersectsRect(player->getBoundingBox()))
			{
				killPlayer();
				bullet->setIsExpended(true);
				deregisterBossBullet(bullet);
			}
		}
	}
	
	
	//checking powerup
	for (int i = 0; i < powerupList.size(); i++)
	{
		Powerup* powerup = powerupList.at(i);
		if (!powerup->getUsed())
		{
			if (powerup->getBoundingBox().intersectsRect(player->getBoundingBox()))
			{
				powerup->collected(player);
				powerup->setUsed(true);
				deregisterPowerup(powerup);
			}
		}
	}
	
	
	// Proper cleanup for destroyed and expended bullets and enemies
	/*
	 Why we need to do this:
	 When you loop an array, it is basically illegal to delete/add
	 anything to the array. Because by doing so, you will alter the size
	 of the array, thus affecting the loop and can potentially crash
	 the game.
	 
	 Thus, we save what we want to delete into another list. Then clear
	 the main array by looping this second list. After the loop, we just
	 need to clear the second list one shot.
	 
	 */
	for (int i = 0; i < expendedBullets.size(); i++)
	{
		bulletList.eraseObject(expendedBullets.at(i));
		expendedBullets.at(i)->removeFromParent();
	}
	expendedBullets.clear();
	
	
	for (int i = 0; i < destroyedEnemies.size(); i++)
	{
		enemyList.eraseObject(destroyedEnemies.at(i));
		destroyedEnemies.at(i)->removeFromParent();
	}
	destroyedEnemies.clear();
	
	
	for (int i = 0; i < expendedBossBullets.size(); i++)
	{
		bossBulletList.eraseObject(expendedBossBullets.at(i));
		expendedBossBullets.at(i)->removeFromParent();
	}
	expendedBossBullets.clear();
	
	for (int i = 0; i < usedPowerups.size(); i++)
	{
		powerupList.eraseObject(usedPowerups.at(i));
		usedPowerups.at(i)->removeFromParent();
	}
	usedPowerups.clear();
}