//
//  MainMenuScreen.h
//  Shooter-Main
//
//  Created by Alexander Lee on 9/9/14.
//
//

#ifndef __Shooter_Main__MainMenuScreen__
#define __Shooter_Main__MainMenuScreen__

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;
using namespace CocosDenshion;

class MainMenuScreen : public Node
{
public:
	CREATE_FUNC(MainMenuScreen);
	virtual bool init();
	bool tapToBegan(Touch* touch, Event* event);
};

#endif /* defined(__Shooter_Main__MainMenuScreen__) */
