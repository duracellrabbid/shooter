//
//  HUDLayer.h
//  Shooter-Main
//
//  Created by Alexander Lee on 7/9/14.
//
//

#ifndef __Shooter_Main__HUDLayer__
#define __Shooter_Main__HUDLayer__

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

class HUDLayer : public Node
{
protected:
	Vector<Sprite*> lifeSprite;
	CC_SYNTHESIZE_READONLY(int, lifeLeft, LifeLeft);
	

	
public:
	CREATE_FUNC(HUDLayer);
	virtual bool init();
	void reduceLife();
};


#endif /* defined(__Shooter_Main__HUDLayer__) */
