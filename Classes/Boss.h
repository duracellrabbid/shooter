//
//  Boss.h
//  Shooter-Main
//
//  Created by Alexander Lee on 19/9/14.
//
//

#ifndef __Shooter_Main__Boss__
#define __Shooter_Main__Boss__

#include <stdio.h>
#include "Enemy.h"
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;


// enumeration to define the Boss state
enum BOSS_STATE
{
	STATE_ATTACKING = 0,
	STATE_RESTING = 1,
	STATE_RAGE,
	STATE_DYING
};

// Boss is essentially a specialized Enemy, so
// it should inherit from Enemy so that we can process
// its logic similar to Enemy and reuse Enemy's functions
// without rewriting the same logic.
class Boss : public Enemy {
	
protected:
	float timeElapsed; //track shoot time
	float totalTimeElapsed; // track state time
	int currentState; // current state
	void changeToState(int nextState); //function to transit to next state
	virtual void postDeath() {};

public:
	CREATE_FUNC(Boss);
	virtual bool init();
	virtual void move();
	virtual void hitByPlayer();
	virtual void hitByBullet(Bullet* bullet);
	void shoot(float dt);
	void updateState(float dt);
	void rageShoot(float dt);
	void moveToPlayer();

};

#endif /* defined(__Shooter_Main__Boss__) */
