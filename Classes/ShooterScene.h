//
//  ShooterScene.h
//  Shooter
//
//  Created by Alexander Lee on 31/8/14.
//
//

#ifndef __Shooter__ShooterScene__
#define __Shooter__ShooterScene__

#include <iostream>
#include "cocos2d.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Player.h"
#include "HUDLayer.h"
#include "BossBullet.h"
#include "Powerup.h"
using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

class ShooterScene : public Node {
	
protected:
	//Synthesize the list to hold enemy and bullet objects for
	//future processing
	CC_SYNTHESIZE_READONLY(Vector<Enemy*>, enemyList, EnemyList);
	CC_SYNTHESIZE_READONLY(Vector<Bullet*>, bulletList, BulletList);
	CC_SYNTHESIZE_READONLY(Vector<BossBullet*>, bossBulletList, BossBulletList);
	CC_SYNTHESIZE(Vector<Powerup*>, powerupList, PowerupList);
	//trash cans
	Vector<Bullet*> expendedBullets;
	Vector<Enemy*> destroyedEnemies;
	Vector<BossBullet*> expendedBossBullets;
	Vector<Powerup*> usedPowerups;
	
	//synthesize the player ship so that it can be referred in other classes
	CC_SYNTHESIZE_READONLY(Player*, player, Player);

	float timeElapsed;
	Sprite* bg1, *bg2;
	TMXTiledMap* backgroundLayer;
	HUDLayer* hud;
public:
	CREATE_FUNC(ShooterScene);
	bool init();
	void respawnPlayer(); //a function to spawn the player object.

	void scrollBackground(float dt);
	void spawnEnemy(float dt);//spawn an enemy at a certain interval
	void update(float dt); //an update function to check bullet/enemy collision every frame
	
	//register function to place the bullets/enemies to the respective
	//list. note that these functions are implemented in the headerfiles.
	
	// it is ok to implement the functions in the header files.
	// however, it is not encouraged. Normally we do it if the
	// function is a one-liner implementation
	void registerBullet(Bullet* bullet) { bulletList.pushBack(bullet); };
	void registerBossBullet(BossBullet* bullet) { bossBulletList.pushBack(bullet); };
	void registerEnemy(Enemy* enemy) { enemyList.pushBack(enemy); };
	void registerPowerup(Powerup* powerup) { powerupList.pushBack(powerup); };
	
	
	void deregisterBullet(Bullet* bullet);
	void deregisterEnemey(Enemy* enemy);
	void deregisterBossBullet(BossBullet* bullet);
	void deregisterPowerup(Powerup* powerup);
	
	void spawnBoss();
	void killPlayer();
};
#endif /* defined(__Shooter__ShooterScene__) */
